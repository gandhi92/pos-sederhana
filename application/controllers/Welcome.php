<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Login_model");
	}	

	public function index(){
		if (count($this->session->userdata())>1) {
			$this->load->view('dashboard');

		}
		else{
			$this->load->view('welcome_message');
		}
	}

	public function login(){
		$uname = $this->input->post('uname');
		$pwd = $this->input->post('pwd');
		$ret = $this->Login_model->cek_user($uname,md5($pwd));
		if (count($ret)>0) {
			$data = array(
				"id"=>$ret->id,
				"username"=>$ret->username,
				"foto"=>$ret->foto,
				"no_hp"=>$ret->no_hp,
				"email"=>$ret->email
			);
			$this->session->set_userdata($data);
			redirect("Welcome");
		}
		else{
			redirect("Welcome");
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect("Welcome");
	}
}
